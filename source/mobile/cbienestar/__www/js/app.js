// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', [
    'ionic',
    'starter.controllers',
    'starter.services', 
    'ionic-material', 
    'ionMdInput', 
    'ngStorage', 
    'ngOpenFB',
    'restangular'])

.constant('configVar', {
    apiBase: 'http://127.0.0.1:8000/api/v1/',
    apiSock: 'http://127.0.0.1:8080/notifications',
    appId: '1674062792919040',
    appSchema: 'cbapp'
})

.factory('tokenInterceptor', function ($q, $window, configVar, $localStorage) {
    return {
      request: function(config) {
        if(config.url.indexOf(configVar.apiBase) !== -1 && config.url.indexOf('login') == -1 && config.url.indexOf('signup') == -1) {
            config.headers.Authorization = 'Token ' + $localStorage.token;            
        }
        return config;
      },
      response: function(response) {
        return response || $q.when(response);
      }
    };
})

.run(function($ionicPlatform, ngFB, configVar, Restangular, $localStorage, Auth) {
    $ionicPlatform.ready(function() {
        ngFB.init({appId: configVar.appId});
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
        
        Restangular.setErrorInterceptor(function(response, deferred, responseHandler){
            //temporary hack until we move perms check to models
            if(response.status==403)  {
                Auth.Logout();
                return false;
            }
            return true;
        });
    });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, 
    RestangularProvider, configVar, $httpProvider) {

    // Turn off caching for demo simplicity's sake
    $ionicConfigProvider.views.maxCache(0);
    
    // config restangular
    RestangularProvider.setBaseUrl(configVar.apiBase);
    RestangularProvider.setRequestSuffix('/');

    RestangularProvider.setResponseExtractor(function(response, operation, what, url) {
        // This is a get for a list
          var newResponse;
          if (operation === "getList") {
            // Here we're returning an Array which has one special property metadata with our extra information
            if(response.results) {
              newResponse = response.results;
              newResponse.metadata = {count: response.count, previous: response.previous, next: response.next};
            } else {
              newResponse = response;
            }
          } else {
            // This is an element
            newResponse = response;
          }
          return newResponse;
      });

    $httpProvider.interceptors.push('tokenInterceptor');

    /*
    // Turn off back button text
    $ionicConfigProvider.backButton.previousTitleText(false);
    */

    $stateProvider
    
    .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
    })

    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl',
        onEnter: function($state, $location, Auth){
            if(!Auth.isLoggedIn()){
                $location.path('/login');
            }
        }
    })

    .state('app.activity', {
        url: '/activity',
        views: {
            'menuContent': {
                templateUrl: 'templates/activity.html',
                controller: 'ActivityCtrl'
            },
            'fabContent': {
                template: '<button id="fab-activity" class="button button-fab button-fab-top-right expanded button-energized-900 flap"><i class="icon ion-paper-airplane"></i></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                        document.getElementById('fab-activity').classList.toggle('on');
                    }, 200);
                }
            }
        }
    })

    .state('app.friends', {
        url: '/friends',
        views: {
            'menuContent': {
                templateUrl: 'templates/friends.html',
                controller: 'FriendsCtrl'
            },
            'fabContent': {
                template: '<button id="fab-friends" class="button button-fab button-fab-top-left expanded button-energized-900 spin"><i class="icon ion-chatbubbles"></i></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                        document.getElementById('fab-friends').classList.toggle('on');
                    }, 900);
                }
            }
        }
    })

    .state('app.gallery', {
        url: '/gallery',
        views: {
            'menuContent': {
                templateUrl: 'templates/gallery.html',
                controller: 'GalleryCtrl'
            },
            'fabContent': {
                template: '<button id="fab-gallery" class="button button-fab button-fab-top-right expanded button-energized-900 drop"><i class="icon ion-heart"></i></button>',
                controller: function ($timeout) {
                    $timeout(function () {
                        document.getElementById('fab-gallery').classList.toggle('on');
                    }, 600);
                }
            }
        }
    })

    .state('app.main', {
        url: '/main',
        views: {
            'menuContent': {
                templateUrl: 'templates/main.html',
                controller: 'MainCtrl'
            },
            'fabContent': {
                template: ''
            }
        }
    })

    .state('step1', {
        url: '/step1',
        templateUrl: 'templates/step1.html',
        controller: 'Step1Ctrl'
    })

    .state('step2', {
        url: '/step2',
        templateUrl: 'templates/step2.html',
        controller: 'Step2Ctrl'
    })

    .state('app.profile', {
        url: '/profile',
        views: {
            'menuContent': {
                templateUrl: 'templates/profile.html',
                controller: 'ProfileCtrl'
            },
            'fabContent': {
                template: '<button id="fab-profile" class="button button-fab button-fab-bottom-right button-energized-900"><i class="icon ion-plus"></i></button>',
                controller: function ($timeout) {
                    /*$timeout(function () {
                        document.getElementById('fab-profile').classList.toggle('on');
                    }, 800);*/
                }
            }
        }
    })
    ;

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/main');
});
