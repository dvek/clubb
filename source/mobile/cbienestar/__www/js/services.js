'use strict';

angular.module('starter.services', [])

.service('Auth', function($q, Restangular, ngFB, $localStorage, $state) {

    var SignUp = function(user) {
        return Restangular.all('signup').post(user);
    }

    var login = function(user) {
        return Restangular.all('login').post(user);
    }

    var info = function() {
        return Restangular.one('profile').get();
    }

    var infoUser = {};

    var setUser = function(user) {
        infoUser.id = user.id;
        infoUser.name = user.name;
        infoUser.email = user.email;
        infoUser.first_name = user.first_name;
        infoUser.last_name = user.last_name;
        return infoUser;
    }

    return {

        ConnectNormal: function(user) {
            SignUp(user).then(function(data) {
                $localStorage.token = data.token;
                $setUser(data);
                $state.go('app.main'); 
            }, function(response) {
                console.log(response);
            });
        },

        ConnectFacebook: function(token) {
            ngFB.api({
                path: '/me',
                params: {fields: 'id,email,first_name,last_name'}
            }).then(function (user) {
                user['provider'] = 'facebook';
                user['token'] = token;
                SignUp(user).then(function(data) {
                    $localStorage.token = data.token;
                    setUser(data);
                    $state.go('app.main'); 
                }, function(response) {
                    login(user).then(function(data) {
                        $localStorage.token = data.token;
                        $state.go('app.main'); 
                    }, function(response) {
                        console.log(response);
                    });
                });
            }, function (error) {
                alert('Facebook error: ' + error.error_description);
            });
        },

        ConnectGoogle: function(user) {

        },

        Login: function(user) {
            login(user).then(function(data) {
                $localStorage.token = data.token;
                $state.go('app.main'); 
            }, function(response) {
                console.log(response);
            });
        },

        Logout: function(user) {
            $localStorage.token =  null;
            $state.go('login');
        },

        isLoggedIn: function() {
            var token = $localStorage.token || null;
            if(token !== null || token !== '') {
                return true;
            }
            return false;
        },

        getUserInfo: function() {

            var defer = $q.defer();

            if (infoUser.id) {
                defer.resolve(infoUser);
            } else {
                info().then(function(data) {
                    setUser(data);
                    defer.resolve(infoUser);
                });
            }
            return defer.promise;
        },

        list: function() {
            return Restangular.all('users').getList();
        }
    }
    })

;