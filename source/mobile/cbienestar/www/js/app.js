// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', [
  'ionic',
  'ionic.service.core',
  'starter.constants',
  'starter.routes',
  'starter.controllers', 
  'starter.services',
  'ngStorage', 
  'ngOpenFB',
  'restangular'
  ])

.run(function($ionicPlatform, ngFB, configVar, Restangular, $localStorage, Auth) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    ngFB.init({appId: configVar.appId});

    Restangular.setErrorInterceptor(function(response, deferred, responseHandler){
      //temporary hack until we move perms check to models
      if(response.status==403)  {
        console.log('error 403 restangular');
        Auth.Logout();
        return false;
      }
      return true;
    });

  });
})

.factory('tokenInterceptor', function ($q, $window, configVar, $localStorage) {
    return {
      request: function(config) {
        if(config.url.indexOf(configVar.apiBase) !== -1 && config.url.indexOf('login') == -1 && config.url.indexOf('signup') == -1) {
            config.headers.Authorization = 'Token ' + $localStorage.token;            
        }
        return config;
      },
      response: function(response) {
        return response || $q.when(response);
      }
    };
})

.config(function($stateProvider, $urlRouterProvider, 
    RestangularProvider, configVar, $httpProvider) {

  // config restangular
  RestangularProvider.setBaseUrl(configVar.apiBase);
  RestangularProvider.setRequestSuffix('/');

  RestangularProvider.setResponseExtractor(function(response, operation, what, url) {
    // This is a get for a list
    var newResponse;
    if (operation === "getList") {
      // Here we're returning an Array which has one special property metadata with our extra information
      if(response.results) {
        newResponse = response.results;
        newResponse.metadata = {count: response.count, previous: response.previous, next: response.next};
      } else {
        newResponse = response;
      }
    } else {
      // This is an element
      newResponse = response;
    }
    return newResponse;
  });

  $httpProvider.interceptors.push('tokenInterceptor');

});
