'use strict';

angular.module('starter.constants', [])

.constant('configVar', {
    apiBase: 'http://127.0.0.1:8000/api/v1/',
    apiSock: 'http://127.0.0.1:8080/notifications',
    appId: '1674062792919040',
    appSchema: 'cbapp'
})