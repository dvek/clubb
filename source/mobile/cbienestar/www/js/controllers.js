angular.module('starter.controllers', [])

.controller('LoginCtrl', ['$scope', 'ngFB', 'Auth', function($scope, ngFB, Auth) {
  $scope.user = {};

  $scope.Login =  function() {
    Auth.Login($scope.user);
  }

  $scope.fbLogin = function() {
    ngFB.login({scope: 'email'}).then( function (response) {
      if (response.status === 'connected') {
        console.log('Facebook login succeeded');
        Auth.ConnectFacebook(response.authResponse.accessToken);
      } else {
        alert('Facebook login failed');
      }
    });
  }

}])

.controller('SignupCtrl', ['$scope', '$stateParams', '$state', 'Auth', 
  function ($scope, $stateParams, $state, Auth) {

  $scope.user = {}

  $scope.cancel = function() {
    $state.go('login'); 
  }

  $scope.signup = function() {
    Auth.ConnectNormal($scope.user);
  }

}])

.controller('AppCtrl', function($scope, $ionicPopup, Auth) {

  $scope.Logout = function() {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Terminar sesión',
      template: 'Esta seguro que desea terminar la sesión actual?'
    });

    confirmPopup.then(function(res) {
      if(res) {
        Auth.Logout();
      } else {
        console.log('You are not sure');
      }
    });
  };

})

.controller('DashCtrl', function($scope, Auth) {
  Auth.getUserInfo().then(function(data) {
    $scope.user = data;
  });
})

.controller('ClubsCtrl', function($scope, Auth, Club) {
  $scope.clubs = [];
  Club.clubList().then(function(clubs) {
    $scope.clubs = clubs;
  });
})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('MargenCtrl', function($scope) {
 
})

.controller('RegisterCtrl', function($scope, Auth, Club, $state) {

  $scope.club = {};
  $scope.user = {};
  Auth.getUserInfo().then(function(data) {
    $scope.user = data;
  });

  $scope.submit_step1 = function() {
    Auth.infoUser.herbalife_id = $scope.user.herbalife_id;
    Auth.infoUser.herbalife_ref = $scope.user.herbalife_ref;
    $state.go('step2'); 
  }

  $scope.step3new = function() {
    $state.go('step3new'); 
  }

  $scope.step3join = function() {
    $state.go('step3join');
  }

  $scope.create = function() {
    var user = {
      herbalife_id: Auth.infoUser.herbalife_id,
      herbalife_ref: Auth.infoUser.herbalife_ref
    };
    Auth.updateUserInfo(user).then(function(USER) {
      Auth.getUserInfo().then(function() {
        Club.clubList().then(function(data) {
          if(data.lenght > 0) {
            $state.go('app.dash');
          } else {
            data.post($scope.club).then(function(result) {
              $state.go('app.dash');
            });
          }
        });
      });  
    }, function(response) {
      console.log('error when save user profile')
    });   
  }

  $scope.join = function() {
    $state.go('app.dash');
  }
})

.controller('AccountCtrl', function($scope, Auth) {
  Auth.getUserInfo().then(function(data) {
    $scope.user = data;
  });
});
