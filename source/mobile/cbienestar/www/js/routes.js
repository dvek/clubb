angular.module('starter.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl'
  })

  .state('signup', {
    url: '/signup',
    templateUrl: 'templates/signup.html',
    controller: 'SignupCtrl'
  })

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl',
    onEnter: function($state, $location, Auth){
      if(!Auth.isLoggedIn()){
        $location.path('/login');
      } else {
        Auth.getUserInfo().then(function(data) {
          if(!data.is_complete) {
           $state.go('step1');
          }
        });
      }
    }
  })

  .state('app.dash', {
    url: '/dash',
    views: {
            'menuContent': {
                templateUrl: 'templates/dash.html',
                controller: 'DashCtrl'
            },
            'fabContent': ""
        }
  })

  .state('app.chats', {
      url: '/chats',
      views: {
        'menuContent': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
  })
  
  .state('app.chat-detail', {
    url: '/chats/:chatId',
      views: {
        'menuContent': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
  })

  .state('app.account', {
    url: '/account',
    views: {
      'menuContent': {
        templateUrl: 'templates/account.html',
        controller: 'AccountCtrl'
      }
    }
  })

  .state('step1', {
    url: '/step1',
    templateUrl: 'templates/step1.html',
    controller: 'RegisterCtrl'
  })

  .state('step2', {
    url: '/step2',
    templateUrl: 'templates/step2.html',
    controller: 'RegisterCtrl'
  })

  .state('step3new', {
    url: '/step3new',
    templateUrl: 'templates/step3new.html',
    controller: 'RegisterCtrl'
  })

  .state('step3join', {
    url: '/step3join',
    templateUrl: 'templates/step3join.html',
    controller: 'RegisterCtrl'
  })

  .state('app.clubs', {
    url: '/clubs',
    views: {
      'menuContent': {
        templateUrl: 'templates/clubs.html',
        controller: 'ClubsCtrl'
      }
    }
  })

  .state('ediciNClub', {
    url: '/club_edit',
    views: {
      'menuContent': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })

  .state('detalleClub', {
    url: '/club_detail',
    views: {
      'menuContent': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })

  .state('franjaDetalle', {
    url: '/borders_detail',
    views: {
      'menuContent': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })

  .state('busquedaUsuario', {
    url: '/search_user',
    views: {
      'menuContent': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })

  .state('edicionFranja', {
    url: '/borders_edit',
    views: {
      'menuContent': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })

  .state('app.margens', {
    url: '/margens',
    views: {
      'menuContent': {
        templateUrl: 'templates/margens.html',
        controller: 'MargenCtrl'
      }
    }
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/dash');
  
});