from django.contrib import admin

from models import Club, Margen, MargenUse


@admin.register(Club)
class ClubAdmin(admin.ModelAdmin):
    pass


@admin.register(Margen)
class MargenAdmin(admin.ModelAdmin):
    pass


@admin.register(MargenUse)
class MargenUseAdmin(admin.ModelAdmin):
    pass