from rest_framework_nested import routers
from views import ClubViewSet, MargenViewSet
from core.api import SharedAPIRootRouter

router = SharedAPIRootRouter()
router.register(r'clubs', ClubViewSet, base_name='clubs')
router.register(r'margens', MargenViewSet, base_name='margens')

#club_router = routers.NestedSimpleRouter(router, r'clubs', lookup='club')
#club_router.register(r'margens', MargenViewSet, base_name='club-margens')
#club_router.register(r'admins', AdminViewSet, base_name='club-admins')

#margen_router = routers.NestedSimpleRouter(club_router, r'margens', lookup='margen')
#margen_router.register(r'admins', AdminViewSet, base_name='margen-admins')