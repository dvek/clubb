

class ClubDictMixin(object):
    """
    metodos de diccionario para la respuestas
    del api rest
    """

    def dict_values(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'is_active': self.is_active}
