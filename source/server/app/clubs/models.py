from __future__ import unicode_literals
from django.db import models
from django.conf import settings
from django.db import transaction

from core.models import TrackerModel


class Club(TrackerModel):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    location = models.TextField(blank=True, null=True)
    schedule = models.TextField(blank=True, null=True)
    is_active = models.BooleanField(default=False, verbose_name=u'Active')
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, related_name="%(class)s_owner")
    admins = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name="%(class)s_admin")
    members = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name="%(class)s_members")

    def __unicode__(self):
        return u'%s' % self.name

    @transaction.atomic
    def set_active(self):
        self.models.objects.filter(is_active=True).update(is_active=False)
        self.is_active=True
        self.save()


    def dict_values(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'is_active': self.is_active}


class Margen(TrackerModel):
    name = models.CharField(max_length=200)
    club = models.ForeignKey(Club, related_name="margens")
    description = models.TextField(blank=True, null=True)
    admins = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name="%(class)s_admin")
    members = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name="%(class)s_members")

    def __unicode__(self):
        return u'%s' % self.name

    def dict_values(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description
        }


class MargenUse(TrackerModel):
    margen = models.ForeignKey(Margen, null=True, blank=True)
    active = models.BooleanField(default=True)
    close_date = models.DateTimeField(null=True)

    def __unicode__(self):
        return u'%s. active: [%s]' % (self.margen, self.active)