from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes


class MargenPermission(permissions.BasePermission):
    message = 'Adding margen not allowed.'

    def has_permission(self, request, view):
        return True

    def has_object_permission(self, request, view, obj=None):
        return True


class ClubPermission(permissions.BasePermission):
    message = 'Adding margen not allowed.'

    def has_permission(self, request, view):
        return True

    def has_object_permission(self, request, view, obj=None):
        return True