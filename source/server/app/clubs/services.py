from models import Club, Margen


def create_club(name, owner, **kwargs):
    active = not Club.object.filter(is_active=True).exists()
    club = Club.objects.create(
        name=name,
        owner=owner,
        is_active = active
        **kwargs)
    club.admins.add(owner)
    return club

def update_club(qs, *kwargs):
    club = qs.objects.get(id=kwargs.get('id'))

def list_club(qs):
    if qs:
        return qs.values('id', 'name', 'description', 'is_active')
    return []

def list_margen(qs):
    if qs:
        return qs.values('id', 'name', 'description')
    return []

def create_margen(name, club, description, owner, **kwargs):
    margen = Margen.objects.create(
        name=name,
        club=club,
        description=description,
        **kwargs)
    margen.admins.add(owner)
    return club
