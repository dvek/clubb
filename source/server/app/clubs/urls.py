from django.conf.urls import patterns, include, url
from django.conf.urls import patterns, include, url

from api import club_router, margen_router

urlpatterns = [
    url(r'^', include(club_router.urls)),
    url(r'^', include(margen_router.urls))
]
