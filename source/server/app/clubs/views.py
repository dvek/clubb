from django.shortcuts import get_object_or_404
from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from rest_framework.decorators import detail_route

from services import create_club, list_club, list_margen
from models import Club, Margen
from core.views import ManageViewMixin
from permissions import MargenPermission, ClubPermission


class ClubViewSet(viewsets.ViewSet, ManageViewMixin):
    permission_classes = (permissions.IsAuthenticated, ClubPermission)

    def get_queryset(self):
        return Club.objects.filter(owner=self.request.user)

    def list(self, request):
        return Response(list_club(self.get_queryset()))

    def create(self, request):
        name = request.data.get('name')
        description = request.data.get('description')
        user = request.user
        club = create_club(name=name, description=description, owner=user)
        return Response(club.dict_values(), status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        obj = get_object_or_404(self.get_queryset(), pk=pk)
        self.check_object_permissions(request, obj)
        return Response(obj.dict_values())

    def update(self, request, pk=None):
        return Response()

    def destroy(self, request, pk=None):
        self.get_queryset().filter(pk=pk).delete()
        return Response()


class MargenViewSet(viewsets.ViewSet, ManageViewMixin):
    permission_classes = (permissions.IsAuthenticated, MargenPermission)

    def get_queryset(self):
        club = Club.objects.filter(owner=self.request.user, is_active=True).first()
        if club:
            return Margen.objects.filter(club__pk=club.pk)
        return []

    def list(self, request):
        results = list_margen(self.get_queryset())
        return Response(results)

    def create(self, request):
        club = Club.objects.filter(owner=self.request.user, is_active=True).first()
        if club:
            name = request.data.get('name')
            description = request.data.get('description')
            user = request.user
            margen = create_margen(name=name, club=club, description=description, owner=user)
            return Response(margen.dict_values(), status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_404_NOT_FOUND)

    def retrieve(self, request, pk=None):
        obj = get_object_or_404(self.get_queryset(), pk=pk)
        self.check_object_permissions(request, obj)
        return Response(obj.dict_values())

    def update(self, request, pk=None):
        return Response()

    def destroy(self, request, pk=None):
        self.get_queryset().filter(pk=pk).delete()
        return Response()


