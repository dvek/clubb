from django.conf import settings
from rest_framework_nested import routers


def api_urls():
    from importlib import import_module
    for app in settings.INSTALLED_APPS:
        try:
            import_module(app + '.api')
        except (ImportError, AttributeError) as ex:
            pass
    return SharedAPIRootRouter.shared_router.urls


class SharedAPIRootRouter(routers.SimpleRouter):
    shared_router = routers.DefaultRouter()

    def register(self, *args, **kwargs):
        self.shared_router.register(*args, **kwargs)
        super(SharedAPIRootRouter, self).register(*args, **kwargs)
