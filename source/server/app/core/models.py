from __future__ import unicode_literals

from django.db import models


class TrackerModel(models.Model):
    """
    Abstract class model with fields cdate and udate
    for tracking purpouses
    """
    cdate = models.DateTimeField(auto_now_add=True)
    udate = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
