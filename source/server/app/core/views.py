from django.shortcuts import get_object_or_404
from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from rest_framework.decorators import detail_route

from security.services import get_user


class ManageViewMixin(object):

    @detail_route(methods=['post'], permission_classes=[permissions.IsAuthenticated])
    def set_admin(self, request, pk=None, **kwargs):
        user = get_user(request.data.get('user_id'))
        if not user:
            return Response(status=status.HTTP_404_NOT_FOUND)
        obj = get_object_or_404(self.get_queryset(), pk=pk)
        obj.admins.add(user)
        return Response()

    @detail_route(methods=['post'], permission_classes=[permissions.IsAuthenticated])
    def set_member(self, request, pk=None, **kwargs):
        user = get_user(request.data.get('user_id'))
        if not user:
            return Response(status=status.HTTP_404_NOT_FOUND)
        obj = get_object_or_404(self.get_queryset(), pk=pk)
        obj.members.add(user)
        return Response()

    @detail_route(methods=['post'], permission_classes=[permissions.IsAuthenticated])
    def unset_admin(self, request, pk=None, **kwargs):
        user = get_user(request.data.get('user_id'))
        if not user:
            return Response(status=status.HTTP_404_NOT_FOUND)
        obj = get_object_or_404(self.get_queryset(), pk=pk)
        obj.admins.remove(user)
        return Response()

    @detail_route(methods=['post'], permission_classes=[permissions.IsAuthenticated])
    def unset_member(self, request, pk=None, **kwargs):
        user = get_user(request.data.get('user_id'))
        if not user:
            return Response(status=status.HTTP_404_NOT_FOUND)
        obj = get_object_or_404(self.get_queryset(), pk=pk)
        obj.members.remove(user)
        return Response()
