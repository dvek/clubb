from views import UserViewSet
from core.api import SharedAPIRootRouter


router = SharedAPIRootRouter()
router.register(r'users', UserViewSet, base_name='users')