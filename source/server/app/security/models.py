from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.utils import timezone
from rest_framework.authtoken.models import Token

from managers import MyUserManager
from core.models import TrackerModel


class MyUser(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField(max_length=254, unique=True, verbose_name=u'Email')
    first_name = models.CharField(max_length=50, null=True, verbose_name=u'First Name')
    last_name = models.CharField(max_length=150, null=True, verbose_name=u'Last Name')
    is_active = models.BooleanField(default=False, verbose_name=u'Active')
    is_staff = models.BooleanField(default=False, verbose_name=u'Is Staff')
    date_joined = models.DateTimeField(default=timezone.now, verbose_name=u'Date of Register')
    role = models.CharField(max_length=15, null=True, blank=True, verbose_name='Role')

    herbalife_id = models.CharField(max_length=15, null=True, blank=True)
    herbalife_ref = models.CharField(max_length=255, null=True, blank=True)
    is_complete = models.BooleanField(default=False)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'

    class Meta:
        verbose_name = 'MyUser'

    def __unicode__(self):
        return self.email

    def get_name(self):
        if not self.last_name:
            return self.email
        return u'%s %s' % (self.first_name, self.last_name)

    def get_short_name(self):
        return self.email

    def generate_token(self):
        return Token.objects.get_or_create(user=self)

    def get_token(self):
        #return Token.objects.get(user=self)
        return self.generate_token()[0].key

    def dict_values(self):
        return {
            'id': self.id,
            'email': self.email,
            'name': self.get_name(),
            'first_name': self.first_name,
            'last_name': self.last_name,
            'token': self.get_token(),
            'herbalife_id': self.herbalife_id,
            'herbalife_ref': self.herbalife_ref,
            'is_complete': self.is_complete}


class Social(TrackerModel):
    user = models.OneToOneField(MyUser, related_name='social')
    provider = models.CharField(max_length=250, null=True, blank=True)
    client_id = models.CharField(max_length=250, null=True, blank=True)
    token = models.CharField(max_length=250, null=True, blank=True)
