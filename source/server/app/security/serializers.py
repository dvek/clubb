from rest_framework import serializers


class SignupSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=255)
    password = serializers.CharField(max_length=128, required=False)
    first_name = serializers.CharField(max_length=50)
    last_name = serializers.CharField(max_length=150)
    role = serializers.CharField(max_length=15, required=False)


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=255)
    password = serializers.CharField(max_length=128, required=False)