from models import MyUser, Social

def list_user():
    return MyUser.objects.values('id', 'email', 'first_name', 'last_name')


def create_user(email, first_name, last_name, password=None, **kwargs):
    if not password:
        #password = MyUser.objects.make_random_password()
        password = kwargs.get('client_id')

    user = MyUser.objects.create(
        email=email,
        first_name=first_name,
        last_name=last_name,
        is_active=kwargs.get('is_active', False))

    user.set_password(password)
    user.save()

    if kwargs.get('provider'):
        Social.objects.create(
            user=user,
            provider=kwargs.get('provider'),
            client_id=kwargs.get('client_id'),
            token=kwargs.get('token'))

    return user


def get_user(user_id):
    return MyUser.objects.filter(id=user_id).first()