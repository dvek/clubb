from django.conf.urls import patterns, include, url
from views import Login, Signup, Profile


urlpatterns = [
    url(r'login', Login.as_view(), name="login"),
	url(r'signup', Signup.as_view(), name="signup"),
	url(r'profile', Profile.as_view(), name="profile")
]
