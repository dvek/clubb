from django.core.cache import cache
from django.shortcuts import get_object_or_404
from django.contrib.auth import authenticate
from rest_framework import viewsets, permissions, status
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

from serializers import SignupSerializer, LoginSerializer
from models import MyUser
from services import list_user, create_user, get_user


class Signup(APIView):
    permission_classes = (AllowAny, )
    serializer_class = SignupSerializer

    def post(self, request):
        print '--- signup ---'
        print request.data
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            try:
                MyUser.objects.get(email=request.data['email'])
                content = {'detail': 'User with this Email address already exists.'}
                return Response(content, status=status.HTTP_409_CONFLICT)

            except MyUser.DoesNotExist:
                kwargs = {}
                
                if request.data.get('provider'):
                    kwargs['token'] = request.data.get('token')
                    kwargs['provider'] = request.data.get('provider')
                    kwargs['client_id'] = request.data.get('id')

                user = create_user(
                    email=serializer.data['email'],
                    first_name=serializer.data['first_name'],
                    last_name=serializer.data['last_name'],
                    password=serializer.data.get('password'),
                    is_active=True,
                    **kwargs)

                content = {'detail': ' Your registration information has been submitted.'}
                return Response(user.dict_values(), status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Custom login with email/password for get token
class Login(APIView):
    permission_classes = (AllowAny, )
    serializer_class = LoginSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            email = serializer.data['email'].lower()
            
            if request.data.get('provider'):
                password = request.data.get('id')
            else:
                password = serializer.data['password']

            user = authenticate(email=email, password=password)

            if user:
                if user.is_active:
                    token, created = Token.objects.get_or_create(user=user)
                    return Response(user.dict_values())
                else:
                    return Response({'detail': 'Sorry, this account is currently inactive.  Please contact the ' + settings.PRODUCT_NAME + ' administrator if you think this is in error.'},
                                    status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response({'detail': 'There was a problem with your email/password combination. Please try again.'},
                                status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response(serializer.errors, status=status.HTTP_401_UNAUTHORIZED)


class Profile(APIView):
    permission_classes = (permissions.IsAuthenticated, )
    
    def get(self, request, format=None):
        user = request.user.dict_values()
        return Response(user)

    def post(self, request, format=None):
        herbalife_id = request.data.get('herbalife_id')
        herbalife_ref = request.data.get('herbalife_ref')
        user = request.user
        user.herbalife_id = herbalife_id
        user.herbalife_ref = herbalife_ref
        user.is_complete = True
        user.save()
        return Response(user.dict_values())


class UserViewSet(viewsets.ViewSet):
    permission_classes = (permissions.IsAuthenticated, )

    def list(self, request):
        list_opt = [user for user in list_user()]
        return Response(list_opt)

    def retrieve(self, request, pk=None):
        user = get_object_or_404(MyUser, pk=pk)
        data = {'id': user.id,
            'email': user.email, 
            'first_name': user.first_name, 
            'last_name': user.last_name}
        return Response(data)
